FROM php:7.1-cli

MAINTAINER london-digital@hogarth-ogilvy.com

RUN apt-get update
RUN apt-get install -y git mercurial zip

RUN apt-get install -y libmcrypt-dev
RUN docker-php-ext-install -j$(nproc) mcrypt
